## Introduction

The Search API Exclude Paragraphs module makes it possible to exclude certain types of Paragraphs from being indexed in search indexes using Search API framework.

The primary use case for this module is excluding Paragraphs from search indexes, for example:

- Paragraphs containing output from Views
- Paragraphs containing only promotional links to other nodes or external sites
- Repetitive features that need not be indexed.

## Requirements

- [Search API](https://www.drupal.org/project/search_api)
- [Paragraphs](https://www.drupal.org/project/paragraphs)

## Installation

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## Configuration

- Add your search server and an index in Search API.
- Enable the "Exclude Paragraphs" processor.
- In the processor settings select the Paragraph types that should be excluded from the active index.

## Maintainers

Current maintainers for Drupal 10:

- Seth Hill (sethhill) - https://www.drupal.org/u/sethhill

