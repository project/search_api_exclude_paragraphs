<?php

namespace Drupal\search_api_exclude_paragraphs\Plugin\search_api\processor;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Excludes entities marked as 'excluded' from being indexes.
 *
 * @SearchApiProcessor(
 *   id = "search_api_exclude_paragraphs",
 *   label = @Translation("Exclude Paragraphs"),
 *   description = @Translation("Excludes selected Paragraph types from being indexed."),
 *   stages = {
 *     "alter_items" = -50
 *   }
 * )
 */
class SearchApiExcludeParagraphs extends ProcessorPluginBase implements PluginFormInterface
{

  use PluginFormTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    return $processor;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array
  {
    return [
      'paragraph_types' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array
  {
    $paragraphs_config = $this->getConfiguration()['paragraph_types'];

    $form['paragraph_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Exclude Paragraph types'),
      '#description' => $this->t('Choose the Paragraph types that should be excluded from this index.'),
      '#default_value' => $paragraphs_config ?? [],
      '#options' => $this->getParagraphTypeOptions(),
      '#multiple' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void
  {
    $values = $form_state->getValues();

    // Remove non-selected values.
    $values['paragraph_types'] = array_values(array_filter($values['paragraph_types']));

    $this->setConfiguration($values);
  }

  /**
   * Get the paragraph type options.
   *
   * @return array
   *   Options array with bundles.
   */
  private function getParagraphTypeOptions(): array
  {
    // Get the paragraph types for the entity type.
    $paragraph_types = \Drupal::entityTypeManager()->getStorage('paragraphs_type')->loadMultiple();
    $options = [];

    foreach ($paragraph_types as $paragraph_type) {
      $options[$paragraph_type->id()] = $paragraph_type->label();
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function alterIndexedItems(array &$items): void
  {
    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item_id => $item) {
      $object = $item->getOriginalObject()->getValue();
      if (!$object instanceof EntityInterface) {
        continue;
      }

      $excluded_paragraphs = $this->getConfiguration()['paragraph_types'];

      // Iterate through the fields on the entity and select those that are paragraph reference fields.
      foreach ($object as $field_name => $field) {
        if (str_starts_with($field_name, 'field_') && $field->getFieldDefinition()->getType() == 'entity_reference_revisions') {
          // Iterate through the paragraphs and remove the ones we don't want indexed.
          foreach ($field as $index => $paragraph) {
            $paragraph = $paragraph->entity;

            if (in_array($paragraph->bundle(), $excluded_paragraphs)) {
              // Remove the paragraph from the index.
              $object->{$field_name}[$index] = NULL;
            }
          }
        }
      }
    }
  }
}
